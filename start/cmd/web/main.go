package main

import (
	"fmt"
	"go-web-application/pkg/handlers"
	"net/http"
)

// it cant changed by enythong else in the application
const PortNumber = ":8080"

func main() {

	http.HandleFunc("/", handlers.Home)
	http.HandleFunc("/about", handlers.About)

	fmt.Println("listening On " + PortNumber)

	http.ListenAndServe(PortNumber, nil)

}
