package handlers

import (
	"go-web-application/pkg/render"
	"net/http"
)

// Home is the home page handler
func Home(w http.ResponseWriter, r *http.Request) {

	render.RenderTemplates(w, "home.html")
}

// about is the about page handler
func About(w http.ResponseWriter, r *http.Request) {

	render.RenderTemplates(w, "about.html")

}
