package render

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

// RenderTemplates render templates using html/template library
func RenderTemplatesT(w http.ResponseWriter, tplName string) {

	var tpl *template.Template
	tpl = template.Must(template.ParseFiles("./templates/"+tplName, "./templates/base-layout.html"))

	err := tpl.Execute(w, nil)

	if err != nil {
		fmt.Println("error parsing templates: ", err)

		return
	}

}

// ttc stands for template cache
var tc = make(map[string]*template.Template)

func RenderTemplates(w http.ResponseWriter, tName string) {

	var tpl *template.Template
	var err error

	// check to see if we allready  have the template in our cache
	if _, inMsp := tc[tName]; !inMsp {

		fmt.Println()
		// need to create the template
		err = createTemplate(tName)

		if err != nil {

			log.Println(err)
		}

		fmt.Println("create template...")

	} else {

		// we have the template in cache
		fmt.Println("using cached template...")

	}

	tpl = tc[tName]
	err = tpl.Execute(w, nil)

	if err != nil {
		log.Panicln(err)
	}

}

func createTemplate(tplName string) error {

	// i'll put one entry for each of the
	// things required to render a template to the web
	// agar layout hamon bishtar shod be ezaye harkdom yek entry toye in
	// slice add mikonim
	templates := []string{
		fmt.Sprintf("./templates/%s", tplName),
		"./templates/base-layout.html",
	}

	// parse the templates
	tmpl, err := template.ParseFiles(templates...)

	if err != nil {
		return err
	}

	// add template to cache (map)
	tc[tplName] = tmpl

	return nil

}
