# Hello World

## Noets

- Notes From Ep021 to Ep026

- the convention in go is that the first thing that should appear in the comment is the **function name**

- in this course we use .go.tpl as a golang template instead of uding html

- we have some dynamic data in our templates

- we need to render a template in our handler

- first we need to parse a template

- putting all of the go codes in a single file will get more complex over time .

- ok then i'm going to put all of my handlers into [handlers.go] file

- go module is basically package management for go it's like using composer for PHP it allows us easuly use external third party package

```go
    go mod init [Name]
```

- conventionally , the name of your application should correspond to where it exists in a git repo for example

- web directory is tipically where Web applications often have their main function .

- all go files that they arerelated to each other must live in same
  directory

- pkg folder is where i'm going to put all the packages that i build for my application

- when you have a function in a package that it's name begins with lowercase letter , that is a private function . or package access function .

- to run the project :

```go
  go run ./cmd/web/*.go
```

##

## Bad Practice

- look at this code :

```go
  // RenderTemplates render templates using html/template library
  func RenderTemplates(w http.ResponseWriter, tplName string) {

    var tpl *template.Template
    tpl = template.Must(template.ParseFiles("./templates/"+tplName, "./templates/base-layout.html"))

    err := tpl.Execute(w, nil)

    if err != nil {
      fmt.Println("error parsing templates: ", err)

      return
    }

  }
```

> **Note :** with this code if i go back to our application and i load that home page this render function goes to the disk and reads home.html and base-layout.html files and then parse them and executes them so that's really inefficient .

- we have to do this every single time someone visits our site .

##

## video 038

- ```go
    var tc = make(map[string]*template.Template)
  ```

- for building a simple template cache we're using a map

- ma darvaghe miyaim az roye layout haye kokhtalef k darim ye template ro misazim va badesh stroe mikonim on ro toye in map k nakhad harbar az disk bekhonim va parse konim

- deghat konid k ma ch template ro dashte bashim toye cache ch majbor bashim besazimesh bayad on ro baad execute konim

- putting all the logic in the single function will make it harder to test !

##
